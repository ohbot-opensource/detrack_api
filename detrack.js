const VehicleService = require('./service/vehicles.service')
const DeliveryService = require('./service/deliveries.service')
const CollectionService = require('./service/collections.service')

const { Item, NotifyItem } = require('./model/item.model')
const { Delivery, DeliveryNotify } = require('./model/delivery.model')
const { Collection, CollectionNotify } = require('./model/collection.model')

module.exports = class Detrack {

    constructor(apiKey) {
        this.ApiKey = apiKey
    }

    /**
     * VehicleService
     */
    createVehicleService() {
        return new VehicleService(this.ApiKey)
    }

    /**
     * DeliveryService
     */
    createDeliveryService() {
        return new DeliveryService(this.ApiKey)
    }

    /**
     * CollectionService
     */
    createCollectionService() {
        return new CollectionService(this.ApiKey)
    }

    // ===========================================================
    /**
     * Item Model
     */
    itemModel(sku, description, quantity) {
        return new Item(sku, description, quantity)
    }

    /**
     * Item Model
     */
    notifyItemModel(sku, description, quantity) {
        return new NotifyItem(sku, description, quantity)
    }


    // ===========================================================
    /**
     * Item Model
     */
    deliveryModel(date, uuid, address, items) {
        return new Delivery(date, uuid, address, items)
    }

    /**
     * Item Model
     */
    deliveryNotifyModel(date, uuid, address, items = []) {
        return new DeliveryNotify(date, uuid, address, items)
    }

    // ===========================================================
    /**
     * Item Model
     */
    collectionModel(date, uuid, address, items = []) {
        return new Collection(date, uuid, address, items)
    }

    /**
     * Item Model
     */
    collectNotifyModel(date, uuid, address, items = []) {
        return new CollectionNotify(date, uuid, address, items)
    }
}
