const BaseService = require('./base.service')
const http = require('../lib/https')

module.exports = class Deliveries extends BaseService{

    /**
     * 建構子
     * @param apiKey
     */
    constructor(apiKey){
        super('deliveries', apiKey)

        this.errorCode = [
            {
                // Invalid argument from request.
                code: 1000,
                errorMsg: '參數錯誤'
            },
            {
                // API key is invalid.
                code: 1001,
                errorMsg: 'API Key 錯誤'
            },
            {
                // Delivery with D.O. # already exists on requested date.
                code: 1002,
                errorMsg: '已經存在相同的訂單'
            },
            {
                // Delivery with D.O. # not found on requested date.
                code: 1003,
                errorMsg: '此訂單不存在'
            },
            {
                // Delivery with D.O. # not editable.
                code: 1004,
                errorMsg: '此訂單不得編輯'
            },
            {
                // Delivery with D.O. # not deletable.
                code: 1005,
                errorMsg: '此訂單不得刪除'
            }
        ]
    }

    /**
     * 新增訂單
     * @param deliveryModel: The delivery Model.
     * @returns {Promise<string>}
     */
    addDeliveries(deliveryModel) {
        return http.post(
            this.BaseUrl + '/create.json',
            deliveryModel,
            this.RequestOption)
    }

    /**
     * 編輯訂單
     * @param deliveryModel: The delivery Model.
     * @returns {Promise<string>}
     */
    editDeliveries(deliveryModel) {
        return http.post(
            this.BaseUrl + '/update.json',
            deliveryModel,
            this.RequestOption)
    }

    /**
     * 刪除單一筆訂單
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @param uuid: The D.O. #. The D.O. # of the collection to delete. Required field.
     * @returns {Promise<string>}
     */
    deleteDeliveries(date, uuid) {

        let requestData = {
            date: date,
            do: uuid
        }

        return http.post(
            this.BaseUrl + '/delete.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 刪除所有訂單
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @returns {Promise<string>}
     */
    deleteAllDeliveries(date) {

        let requestData = {
            date: date
        }

        return http.post(
            this.BaseUrl + '/delete/all.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 檢視單一筆訂單
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @param uuid: The D.O. #. The D.O. # of the collection to view. Required field.
     * @returns {Promise<string>}
     */
    viewDeliveries(date, uuid) {

        let requestData = {
            date: date,
            do: uuid
        }

        return http.post(
            this.BaseUrl + '/view.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 檢視所有訂單
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @returns {Promise<string>}
     */
    viewAllDeliveries(date) {

        let requestData = {
            date: date
        }

        return http.post(
            this.BaseUrl + '/view/all.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 下載訂單POD圖片檔案
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @param uuid: The D.O. #. The D.O. # of the collection to download POD image file for. Required field.
     * @returns {Promise<string>}
     */
    dowloadDeliveriesPODImgFile(date, uuid) {

        let requestData = {
            date: date,
            do: uuid
        }

        return http.post(
            this.BaseUrl + '/pod.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 下載訂單POD的簽名檔案
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @param uuid: The D.O. #. The D.O. # of the collection to download POD signature image file for. Required field.
     * @returns {Promise<object>}
     */
    dowloadDeliveriesPODSignatureImgFile(date, uuid) {

        let requestData = {
            date: date,
            do: uuid
        }

        return http.post(
            this.BaseUrl + '/signature.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 下載訂單POD圖片
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @param uuid: The D.O. #. The D.O. # of the collection to download POD photo $n image file for. Required field.
     * @param index: 第幾張圖片（max: 5）
     * @returns {Promise<object>}
     */
    dowloadDeliveriesPODPhotoImgFile(date, uuid, index) {

        let requestData = {
            date: date,
            do: uuid
        }

        if (index > 5) {
            return Promise.reject({
                success: false,
                data: 'index 超出範圍'
            })
        }

        return http.post(
            this.BaseUrl + '/photo_' + index + '.json',
            requestData,
            this.RequestOption)
    }

    /**
     * 下載訂單POD PDF
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Required field.
     * @param uuid: The D.O. #. The D.O. # of the collection to download POD image file for. Required field.
     * @returns {Promise<object>}
     */
    dowloadDeliveriesPODPdfFile(date, uuid) {

        let requestData = {
            date: date,
            do: uuid
        }

        return http.post(
            this.BaseUrl + '/export.pdf',
            requestData,
            this.RequestOption)
    }

    /**
     * 推播訊息通知提醒
     * @returns {Promise<string>}
     */
    deliveriesPushNotification(deliveryNotifyModel) {
        // return http.post(
        //     this.BaseUrl + '/create.json',
        //     deliveryNotifyModel,
        //     this.RequestOption)
    }

    /**
     * 下載訂單包裝PDF
     * @param date: The collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Optional field.
     * @param uuid: The D.O. #. The D.O. # of the collection to download POD image file for. Required field.
     * @returns {Promise<object>}
     */
    downloadDeliveriesShippingLabelPdfFile(date, uuid) {

        let requestData = {
            date: date,
            do: uuid
        }

        return http.post(
            this.BaseUrl + '/label.pdf',
            requestData,
            this.RequestOption)
    }

    /**
     * 修改訂單時間
     * @param uuid: The D.O. #. This field must be unique for the date. Required field.
     * @param newDate: The new collection date. Format: YYYY-MM-DD e.g. 2014-02-30. Required field.
     * @param oldDate: The current collection date. Format: YYYY-MM-DD e.g. 2014-02-28. Optional.
     *                 When the date is not provided, the system will search for the latest job with the given D.O. #.
     * @returns {Promise<object>}
     */
    changeDateOfDeliveries(uuid, newDate, oldDate = '') {

        let requestData = {
            date: oldDate,
            do: uuid,
            new_date: newDate
        }

        return http.post(
            this.BaseUrl + '/change/date',
            requestData,
            this.RequestOption)
    }
}
