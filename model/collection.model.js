module.exports.Collection = class Collection {

    constructor(date, uuid, address, items = []){
        /**
         * 收集貨物時間
         * The collection date. Format: YYYY-MM-DD e.g. 2014-02-28.
         */
        this.date = date

        /**
         * 收集貨物的UUId
         * The D.O. #. This field must be unique for the date.
         */
        this.do = uuid

        /**
         * 收集貨物地址
         * The collection address. Always include country name for accurate geocoding results.
         */
        this.address = address

        /**
         * 貨物來源：時間
         * The collection time window. This will be displayed in the delivery list view and the delivery detail view.
         */
        this.collection_time = ''

        /**
         * 貨物來源：名字
         * The name of the sender to collect from. This can be a person’s name e.g. John Tan, a company’s name e.g. ABC Inc., or both e.g. John Tan (ABC Inc.)
         */
        this.collect_from = ''

        /**
         * 顧客的電話號碼（可以顯示在App，送貨員可以打電話給此顧客）
         * The phone number of the sender. If specified, the driver can call the sender directly from the app.
         * @type {string}
         */
        this.phone = ''

        /**
         * 完成後回傳email
         * The email address to send customer-facing delivery updates to. If specified, a collection notification will be sent to this email address upon successful collection.
         * @type {string}
         */
        this.notify_email = ''

        /**
         * 完成後回傳url
         * The URL to post collection updates to.
         * @type {string}
         */
        this.notify_url = ''

        /**
         * Assign的人員：名字
         * The name of the vehicle to assign this collection to. This must be spelled exactly the same as your vehicle’s name in your dashboard.
         * @type {string}
         */
        this.assign_to = ''

        /**
         * 其他指示、備註
         * Any special collection instruction for the driver. This will be displayed in the collection detail view.
         * @type {string}
         */
        this.instructions = ''

        /**
         * 地區、區域
         * If you divide your collections into zones, then specifying this will help you to easily filter out the collections by zones in your dashboard.
         * @type {string}
         */
        this.zone = ''

        /**
         * This field is optional. If specified, it should contain an array of item model
         * @type {Array}
         */
        this.items = items
    }
}

module.exports.CollectionNotify = class CollectionNotify {

    constructor(date, uuid, address, items = []){
        /**
         * 收集貨物時間
         * The collection date. Format: YYYY-MM-DD e.g. 2014-02-28.
         */
        this.date = date

        /**
         * 收集貨物的UUId
         * The D.O. #. This field must be unique for the date.
         */
        this.do = uuid

        /**
         * 收集貨物地址
         * The collection address. Always include country name for accurate geocoding results.
         */
        this.address = address

        /**
         * 貨物來源：時間
         * The collection time window. This will be displayed in the delivery list view and the delivery detail view.
         */
        this.collection_time = ''

        /**
         * 貨物來源：名字
         * The name of the sender to collect from. This can be a person’s name e.g. John Tan, a company’s name e.g. ABC Inc., or both e.g. John Tan (ABC Inc.)
         */
        this.collect_from = ''

        /**
         * 顧客的電話號碼（可以顯示在App，送貨員可以打電話給此顧客）
         * The phone number of the sender. If specified, the driver can call the sender directly from the app.
         * @type {string}
         */
        this.phone = ''

        /**
         * 完成後回傳email
         * The email address to send customer-facing delivery updates to. If specified, a collection notification will be sent to this email address upon successful collection.
         * @type {string}
         */
        this.notify_email = ''

        /**
         * 完成後回傳url
         * The URL to post collection updates to.
         * @type {string}
         */
        this.notify_url = ''

        /**
         * Assign的人員：名字
         * The name of the vehicle to assign this collection to. This must be spelled exactly the same as your vehicle’s name in your dashboard.
         * @type {string}
         */
        this.assign_to = ''

        /**
         * 其他指示、備註
         * Any special collection instruction for the driver. This will be displayed in the collection detail view.
         * @type {string}
         */
        this.instructions = ''

        /**
         * 地區、區域
         * If you divide your collections into zones, then specifying this will help you to easily filter out the collections by zones in your dashboard.
         * @type {string}
         */
        this.zone = ''

        /**
         *
         * @type {string}
         */
        this.reason = ''

        /**
         *
         * @type {string}
         */
        this.note = ''

        /**
         *
         * @type {string}
         */
        this.received_by = ''

        /**
         *
         * @type {string}
         */
        this.image = ''

        /**
         *
         * @type {string}
         */
        this.view_image_url = ''

        /**
         *
         * @type {string}
         */
        this.time = ''

        /**
         *
         * @type {string}
         */
        this.pod_lat = ''

        /**
         *
         * @type {string}
         */
        this.pod_lng = ''

        /**
         *
         * @type {string}
         */
        this.pod_address = ''

        /**
         * This field is optional. If specified, it should contain an array of item model
         * @type {Array}
         */
        this.items = items
    }
}
