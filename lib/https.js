const axios = require('axios')

/**
 * Post
 * @param url
 * @param postData
 * @param requestOption
 * @returns {Promise<any>}
 */
function post(url, postData, requestOption) {
    return new Promise((resolve, reject) => {
        axios.post( url, postData,
            {
                headers: requestOption
            }
        )
            .then(response => {
                console.log(response.data)
                if (response.data.info.status !== 'ok') {
                    reject(response.data.info.error)
                } else if (response.data.info.failed > 0) {
                    reject(response.data.results)
                } else {
                    resolve(response.data)
                }
            })
            .catch(err => {
                reject(err)
            })
    })
}

module.exports = {
    post: post
}
